#!/usr/bin/env python3
import time
from datetime import datetime
import json
import logging
import app

# Loggerを初期化
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                    level=logging.INFO)
log = logging.getLogger(__name__)

class WS:
    def __init__(self):
        self.WS_STATE = {
                "w1": False,
                "w2": False,
                "w3": False,
                "w4": False,
                "w5": False,
                "w6": False,
                "w7": False,
                "w8": False,
                "w9": False
                }

    def increment_ws(self, index: int):

        if index == 1:
            self.WS_STATE["w1"] = True
            index = 2
        elif index == 2:
            self.WS_STATE["w2"] = True
            index = 3
        elif index == 3:
            self.WS_STATE["w3"] = True
            index = 4
        elif index == 4:
            self.WS_STATE["w4"] = True
            index = 5
        elif index == 5:
            self.WS_STATE["w5"] = True
            index = 6
        elif index == 6:
            self.WS_STATE["w6"] = True
            index = 7
        elif index == 7:
            self.WS_STATE["w7"] = True
            index = 8
        elif index == 8:
            self.WS_STATE["w8"] = True
            index = 9
        elif index == 9:
            self.WS_STATE["w9"] = True
            index = 1
        return index

class PROCESSING:
    def __init__(self):
        self.oven = False

    def isTrue(self):
        self.oven = True

    def isFalse(self):
        self.oven = False

class SORTING:
    def __init__(self):
        self.SORTING_STATE = {
        "red": False,
        "blue": False,
        "white": False
        }

    def increment_sorting(self, index: int):
        if index == 1:
            self.SORTING_STATE[app.COLOR1] = True
            index = 2
        elif index == 2:
            self.SORTING_STATE[app.COLOR2] = True
            index = 3
        elif index == 3:
            self.SORTING_STATE[app.COLOR3] = True
            index = 4
        elif index == 4:
            self.SORTING_STATE[app.COLOR1] = True
            index = 5
        elif index == 5:
            self.SORTING_STATE[app.COLOR2] = True
            index = 6
        elif index == 6:
            self.SORTING_STATE[app.COLOR3] = True
            index = 7
        elif index == 7:
            self.SORTING_STATE[app.COLOR1] = True
            index = 8
        elif index == 8:
            self.SORTING_STATE[app.COLOR2] = True
            index = 9
        elif index == 9:
            self.SORTING_STATE[app.COLOR3] = True
            index = 1
        return index

def create_value_json(ws, processing, sorting):

    value = {
        "time": datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
        "ws": [
            { "cycleid": 0, "ballid": 0, "name": "w1", "run": ws.WS_STATE["w1"] },
            { "cycleid": 0, "ballid": 1, "name": "w2", "run": ws.WS_STATE["w2"] },
            { "cycleid": 0, "ballid": 2, "name": "w3", "run": ws.WS_STATE["w3"] },
            { "cycleid": 1, "ballid": 0, "name": "w4", "run": ws.WS_STATE["w4"] },
            { "cycleid": 1, "ballid": 1, "name": "w5", "run": ws.WS_STATE["w5"] },
            { "cycleid": 1, "ballid": 2, "name": "w6", "run": ws.WS_STATE["w6"] },
            { "cycleid": 2, "ballid": 0, "name": "w7", "run": ws.WS_STATE["w7"] },
            { "cycleid": 2, "ballid": 1, "name": "w8", "run": ws.WS_STATE["w8"] },
            { "cycleid": 2, "ballid": 2, "name": "w9", "run": ws.WS_STATE["w9"] }
            ],
    "arm-starting-point": True,
    "auto-drive-mode-start": True,
    "oven": processing.oven,
    "sorting": [
            { "color": "red", "status": sorting.SORTING_STATE["red"] },
            { "color": "blue", "status": sorting.SORTING_STATE["blue"] },
            { "color": "white", "status": sorting.SORTING_STATE["white"] }
            ]
    }
    value_json = json.dumps(value).encode("utf-8")
    return value_json

def create_plc_data():
    global ws
    global processing
    global sorting

    app.read_val_from_dotenv()

    ws_index = 1
    sorting_index = 1

    while True:

        # 倉庫部制御PLC
        log.info(f"Warehouse data")
        log.info(f"------------------------------------------")
        # class initialize
        ws = WS()
        processing = PROCESSING()
        sorting = SORTING()

        ws_index = ws.increment_ws(ws_index)
        value_json = create_value_json(ws,processing,sorting)

        app.publish(value_json)
        log.info(f"data: {value_json}")
        log.info(f"------------------------------------------")
        time.sleep(int(app.WS_TIMER))

        # 加工部制御PLC
        log.info(f"Processing data")
        log.info(f"------------------------------------------")

        processing.isTrue()
        value_json = create_value_json(ws,processing,sorting)
        app.publish(value_json)
        log.info(f"data: {value_json}")
        log.info(f"------------------------------------------")
        time.sleep(int(app.PROCESSING_TIMER))

        processing.isFalse()
        value_json = create_value_json(ws,processing,sorting)
        app.publish(value_json)
        log.info(f"data: {value_json}")
        log.info(f"------------------------------------------")
        time.sleep(int(app.PROCESSING_TIMER))

        # 仕分け部制御PLC
        log.info(f"Sorting data")
        log.info(f"------------------------------------------")
        sorting_index = sorting.increment_sorting(sorting_index)
        value_json = create_value_json(ws,processing,sorting)
        app.publish(value_json)
        time.sleep(int(app.SORTING_TIMER))
        log.info(f"data: {value_json}")
        log.info(f"------------------------------------------")