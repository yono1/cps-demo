#!/bin/bash

home=$(pwd)
baseapp="rhsc"
username=$1
password=$2
service=$3
version=$4

if [ -z $version ]; then
    version=v1
fi

public_container_path="quay.io/${username}"

for servicename in $(cat image.list | grep -v "#" | grep "${service}")
do
    path="${servicename}/src"
    cd ${home}/${path}
    podman build -t ${servicename}:${version} -f Containerfile
    imageId=$(podman images | grep "localhost/${servicename}" | awk '{print $3}' | uniq)
    echo $imageId

    podman tag ${imageId} ${public_container_path}/${baseapp}/${servicename}:${version}
    podman login ${public_container_path}/${baseapp}/${servicename} --username ${username} --password ${password}
    podman push ${public_container_path}/${baseapp}/${servicename}:${version}
done