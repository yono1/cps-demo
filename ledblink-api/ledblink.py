#!/usr/bin/env python3
import RPi.GPIO as GPIO
import os
from datetime import datetime
import logging
import json
from flask import Flask,request,make_response

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
SERVICE_PORT = "5000" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
RED_PIN = "17" if (os.environ.get("RED_PIN") is None) else os.environ.get("RED_PIN")
BLUE_PIN = "27" if (os.environ.get("BLUE_PIN") is None) else os.environ.get("BLUE_PIN")
WHITE_PIN = "22" if (os.environ.get("WHITE_PIN") is None) else os.environ.get("WHITE_PIN")

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# initial LED state
LED_STATE = {
    "red": "off",
    "blue": "off",
    "white": "off"
    }

# GPIO_PIN Array
GPIO_PIN = {
    "red": int(RED_PIN),
    "blue": int(BLUE_PIN),
    "white": int(WHITE_PIN)
    }

# Turn On LED
def turn_on( LedPin: int, Color: str ):
    GPIO.output(LedPin, GPIO.LOW)
    LED_STATE[ Color ] = "on"

# Tunr Off LED
def turn_off( LedPin: int, Color: str ):
    GPIO.output(LedPin, GPIO.HIGH)
    LED_STATE[ Color ] = "off"
    # Release resource
    GPIO.cleanup()

# Get Led Status
@app.route("/led", methods=["GET"])
def get_led_state():
    return LED_STATE

# Change Led Status
@app.route("/led", methods=["POST"])
def change_led_state():
    # POSTデータを取得
    data = json.loads( request.data )
    Color = str(data["Color"])
    State = str(data["State"])

    LedPin = GPIO_PIN[ Color ]

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LedPin, GPIO.OUT, initial=GPIO.HIGH)
    log.info(f"POST data, LED_COLOR:{Color}, LED_PIN: {LedPin}, STATE:{State}")

    if State == "on":
        turn_on(LedPin, Color)
    else:
        turn_off(LedPin, Color)

    return LED_STATE

####################################
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))

