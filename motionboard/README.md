# Motion Boardコンテナイメージをビルド

注意: コンテナイメージとしてのビルドは現状サポートされていません。

1. MotionBoard_6.4_dl.isoとアクティベーションキーを入手
2. installerディレクトリに、 MotionBoard_6.4_dl.isoの中身を解凍
3. コンテナイメージビルド

```
export ACTIVATION_KEY=xxx
podman build -t motionboard:6.4 --build-arg ACTIVATION_KEY=${ACTIVATION_KEY} -f Containerfile
```