#!/usr/bin/env python3
import os
from datetime import datetime
import logging
import json
import requests
import app

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

cycleid=0
prev_cycleid=0
sorting_index = 0
prev_sorting_index = 0
target_cycleid = []
ballid=0
lock = 0
initFlag = True
predicted_color = []
predicted_color_list = [[],[],[]]
result_color_list = [[],[],[]]
result_color = []

class PLC:
    def __init__(self, data):
        self.data = data

    def get_ws_index(self):
        global ws_index
        try:
            ws_index = self.data['ws_index']
        except KeyError:
            ws_index = -1
        return ws_index

    def get_sorting_index(self):
        global sorting_index
        try:
            sorting_index = self.data['sorting_index']
        except KeyError:
            sorting_index = -1
        return sorting_index

    def get_cycleid(self):
        global cycleid
        try:
            cycleid = self.data['cycleid']
        except KeyError:
            cycleid = 0
        return cycleid

    def get_ballid(self):
        global ballid
        try:
            ballid = self.data['ballid']
        except KeyError:
            ballid = -1
        return ballid

    def get_factory_start_trigger(self):
        global factory_start_flag
        try:
            factory_start_flag = self.data['auto-drive-mode']
        except KeyError:
            factory_start_flag = False
        return factory_start_flag

    def get_cycle_update_flag(self):
        global cycle_update_flag
        try:
            cycle_update_flag = self.data['arm-position']
        except KeyError:
            cycle_update_flag = False
        return cycle_update_flag

    def get_predicted_color(self):
        global predicted_color_list
        try:
            predicted_color_list = self.data['predicted_color_list']
        except KeyError:
            predicted_color_list = [["","",""],["","",""],["","",""]]
        return predicted_color_list

    def get_result_color(self):
        global result_color_list
        try:
            result_color_list = self.data['result_color_list']
        except KeyError:
            result_color_list = [["","",""],["","",""],["","",""]]
        return result_color_list

def deep_validation( factory_start_flag, cycle_update_flag, sorting_index, cycleid: int, predicted_color: list, result_color: list):

    global lock
    global prev_sorting_index
    global prev_cycleid
    global initFlag

    if factory_start_flag and initFlag:
        model_init()
        initFlag = False
        log.info(f'model is initialized because drive mode changes manual to auto')
    elif sorting_index == 8:
        model_init()
        log.info(f'model is initialized because sorting_index == 8')

    if factory_start_flag is False:
        initFlag = True

    log.info(f'[Quality] quality of cycleid: {cycleid}, {predicted_color} <-> {result_color}')
    ng = 0

    for color_index,color in enumerate(result_color):
        if color:
            if predicted_color[color_index] == color:
                log.info(f'[Quality] quality of cycleid: {cycleid}-{color_index} is ok: {predicted_color[color_index]} <-> {color}')
                lock = 0
            else:
                ng += 1
                log.info(f'[Quality] quality of cycleid: {cycleid}-{color_index} is NG count {ng}: {predicted_color[color_index]} <-> {color}')

    if ng >= app.MODEL_CREATE_THRESHOLD and ( prev_sorting_index != sorting_index ) and lock == 0:
        if cycle_update_flag:
            create_model( cycleid + 1 )
            log.info(f'Arm position: {cycle_update_flag} <> True, NG: {ng} >= {app.MODEL_CREATE_THRESHOLD}, prev_sorting_index: {prev_sorting_index} / sorting_index: {sorting_index} -> so predictive model update has been completed!')
            lock = 1
    else:
        log.info(f'Arm position: {cycle_update_flag} <> False, NG: {ng} / {app.MODEL_CREATE_THRESHOLD}, prev_sorting_index: {prev_sorting_index} / sorting_index: {sorting_index} -> currently updating model is locked.')

    prev_cycleid = cycleid
    prev_sorting_index = sorting_index


def check_plc_data( plc ):
    global factory_start_flag, cycle_update_flag, cycleid, ballid
    global predicted_color_list, result_color_list

    # 現在の倉庫の状況を取得
    factory_start_flag = plc.get_factory_start_trigger()
    cycle_update_flag = plc.get_cycle_update_flag()
    sorting_index = plc.get_sorting_index()
    cycleid = plc.get_cycleid()
    ballid = plc.get_ballid()
    predicted_color_list = plc.get_predicted_color()
    result_color_list = plc.get_result_color()

    deep_validation( factory_start_flag, cycle_update_flag, sorting_index, cycleid, predicted_color_list[cycleid], result_color_list[cycleid] )


def create_model(cycleid):
    global target_cycleid

    if cycleid > app.PER_CYCLES:
        target_cycleid = list(range(0,1))
    else:
        target_cycleid = list(range(cycleid, app.PER_CYCLES))

    for value in target_cycleid:
        url = app.MODEL_GEN_ADDRESS_API_ENDPOINT_URL+str(value)
        log.info(f'call API endpoint -> {url}')
        try:
            res = requests.post(url, timeout=app.MODEL_GEN_TIMEOUT)
            if res.status_code == 200:
                log.info(f'cycleid: {value}: Model update is successful')
            else:
                log.info(f'cycleid: {value}: Failed to connect model-gen API')
        except Exception as err:
            log.info(f'cycleid: {value}: failed to call model-gen api: {err}')

def model_init():

    # update initial model
    url = app.MODEL_GEN_ADDRESS+"/update_all"
    try:
        res = requests.post(url, timeout=app.MODEL_GEN_TIMEOUT)
        if res.status_code == 200:
            log.info(f'Model is initialized')
        else:
            log.info(f'Failed to connect model-gen API')
    except Exception as err:
        log.info(f'failed to call model-gen api: {err}')