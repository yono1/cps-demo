#!/usr/bin/env python3
from kafka import KafkaConsumer
import logging
import json
import app
import handler

# Loggerを初期化
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                    level=logging.INFO)
log = logging.getLogger(__name__)

def start():

    # Kafka接続
    consumer = KafkaConsumer(
        app.SUB_TOPIC,
        bootstrap_servers=[app.KAFKA_BOOTSTRAP_SERVER])

    for msg in consumer:
        plc = handler.PLC (json.loads(msg.value))
        handler.check_plc_data( plc )