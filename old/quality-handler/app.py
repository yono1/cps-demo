#!/usr/bin/env python3
import os
import logging
from dotenv import load_dotenv
import mqtt_connect
import kafka_connect

# Loggerを初期化
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                    level=logging.INFO)
log = logging.getLogger(__name__)

def read_val_from_dotenv():
    # Load environment variables from .env file
    load_dotenv()

    # Get the value of a specific environment variable
    global MODEL_GEN_ADDRESS
    global MODEL_GEN_ADDRESS_API_ENDPOINT_URL
    global SUB_TOPIC
    global MQTT_BROKER_ADDRESS
    global MQTT_PORT
    global KAFKA_USE
    global KAFKA_BOOTSTRAP_SERVER
    global PER_CYCLES
    global MODEL_CREATE_THRESHOLD
    global MODEL_GEN_TIMEOUT

    MODEL_GEN_ADDRESS = os.getenv('MODEL_GEN_ADDRESS')
    MODEL_GEN_ADDRESS_API_ENDPOINT_URL = MODEL_GEN_ADDRESS+"/update_next_model/"
    SUB_TOPIC = os.getenv('SUB_TOPIC')
    MQTT_BROKER_ADDRESS = os.getenv('MQTT_BROKER_ADDRESS')
    MQTT_PORT = int(os.getenv('MQTT_PORT'))
    KAFKA_USE = os.getenv('KAFKA_USE')
    KAFKA_BOOTSTRAP_SERVER = os.getenv("KAFKA_BOOTSTRAP_SERVER")
    PER_CYCLES = int(os.getenv('PER_CYCLES'))
    MODEL_CREATE_THRESHOLD = int(os.getenv('MODEL_CREATE_THRESHOLD'))
    MODEL_GEN_TIMEOUT = float(os.getenv('MODEL_GEN_TIMEOUT'))


def publish(value_json):
    read_val_from_dotenv()

    if KAFKA_USE:
        kafka_connect.publish_to_kafka(value_json)
    else:
        mqtt_connect.publish_to_mqtt(value_json)

def start():
    read_val_from_dotenv()

    if KAFKA_USE:
        log.info(f"Mode: Kafka")
        kafka_connect.start()
    else:
        log.info(f"Mode: MQTT")
        mqtt_connect.start()