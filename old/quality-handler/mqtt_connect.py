#!/usr/bin/env python3
import paho.mqtt.client as mqtt
import logging
import json
import app
import handler

# Loggerを初期化
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                    level=logging.INFO)
log = logging.getLogger(__name__)

# ブローカーに接続できたときの処理
def on_connect(client, userdata, flag, rc):
  log.info(f'Connected with result code {str(rc)}')
  client.subscribe(app.SUB_TOPIC)

# ブローカーが切断したときの処理
def on_disconnect(client, userdata, rc):
  if  rc != 0:
     log.info(f'Unexpected disconnection.')

# publishが完了したときの処理
def on_publish(client, userdata, mid):
  log.info(f'Publish successfully')

# メッセージが届いたときの処理
def on_message(client, userdata, msg):
    plc = handler.PLC (json.loads(msg.payload))
    handler.check_plc_data( plc )

def publish_to_mqtt(value_json):
   client.publish(app.PUB_TOPIC,value_json)

def start():
    global client

    # MQTTの接続設定
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    client.on_publish = on_publish

    client.connect( app.MQTT_BROKER_ADDRESS, int(app.MQTT_PORT), 60)
    client.loop_forever()