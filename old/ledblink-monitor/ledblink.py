#!/usr/bin/env python3
from datetime import datetime
import logging
import json
import datetime
import requests
import app

cycleid=0
ballid=0
result_color_list = [["","",""],["","",""],["","",""]]
predicted_color_list = [["","",""],["","",""],["","",""]]
current_result_color = ""
current_predicted_color = ""
# predicted_color_list = []

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# HTTPパラメータ
headers = {"content-type": "application/json"}

# PLCのレジスタデータを取得するためのクラス
class PLC:
    def __init__(self, data):
        self.data = data
        self.color_index = { "1": "white", "2": "red", "3": "blue"}

    # 自動モードの時に True
    def factory_start_trigger(self):
        global factory_start_flag
        try:
            factory_start_flag = self.data['auto-drive-mode']
        except KeyError:
            factory_start_flag = False
        return factory_start_flag

    # スタッカトレーン走行軸原点
    def check_arm_position(self):
        global arm_position
        try:
            arm_position = self.data['arm-position']
        except KeyError:
            arm_position = False
        return arm_position

    # 出庫カウント
    def check_ws_index(self):
        global ws_index
        try:
            ws_index = self.data['ws-index']
        except KeyError:
            ws_index = -1
        return ws_index

    # 廃捨カウント
    def check_sorting_index(self):
        global sorting_index
        try:
            sorting_index = self.data['sorting-index']
        except KeyError:
            sorting_index = -1
        return sorting_index

    # サイクルID(整数)
    def check_cycleid(self):
        global cycleid
        try:
            cycleid = int(self.data['cycleid'])
        except KeyError:
            cycleid = 0

        return cycleid

    # ボールID(整数)
    def check_ballid(self):
        global ballid
        try:
            ballid = int(self.data['ballid'])
        except KeyError:
            ballid = 0

        return ballid

    # 仕分け結果から参照
    def get_result_color(self):
        global ws_index
        global cycleid
        global ballid
        global result_color_list
        global current_result_color

        # 初期化
        try:
            # 初期状態は出庫カウントがマイナスを想定
            if ws_index < 0:
                result_color_list = [["","",""],["","",""],["","",""]]
                current_result_color = ""
        except KeyError:
            result_color_list = [["","",""],["","",""],["","",""]]
            current_result_color = ""

        try:
            ws_list = self.data['ws']
        except KeyError:
            log.info(f'could not capture a warehouse data')

        current_ws = list(filter(lambda item : item['cycleid'] == cycleid and item['ballid'] == ballid, ws_list))
        if current_ws:
            try:
                color = [ x['sorting'] for x in current_ws]
                result_color_list[cycleid][ballid] = self.color_index[str(color[0])]
                current_result_color = self.color_index[str(color[0])]
            except KeyError:
                result_color_list[cycleid][ballid] = ""
                current_result_color = ""

        return current_result_color, result_color_list

# LED制御のためのクラス
class LED:
    def __init__(self):
        self.state = { "red": "off", "blue": "off", "white": "off" }
        self.gpio = { "red": app.RED_PIN, "blue": app.BLUE_PIN, "white": app.WHITE_PIN }

    # Turn On LED
    def turn_on( self, color: str ):
        post_data = { "Color": color, "State": "on" }
        try:
            res = requests.post(app.LED_API_ENDPOINT_URL, data=json.dumps(post_data), timeout=app.LED_API_TIMEOUT)
            if res.status_code == 200:
                self.state[ color ] = "on"
                log.info(f'LED turns onto {color}')
                log.info(f'LED state is : -> {self.state[ color ]}')
        except Exception as err:
            self.state[ color ] = "fail"
            log.info(f'failed to call ledblinkapi: {err}')

    # Tunr Off LED
    def turn_off( self, color: str ):
        post_data = { "Color": color, "State": "off" }
        try:
            res = requests.post(app.LED_API_ENDPOINT_URL, data=json.dumps(post_data),timeout=app.LED_API_TIMEOUT)
            if res.status_code == 200:
                self.state[ color ] = "off"
                log.info(f'LED color: {color} turns off')
                log.info(f'LED state is : -> {self.state[ color ]}')
            else:
                self.state[ color ] = "fail"
                log.info(f'LED control has failed')
        except Exception as err:
            self.state[ color ] = "fail"
            log.info(f'failed to call ledblinkapi: {err}')

# モデルサーバから期待される色配列を取得
def get_all_predicted_color():
    url = app.MODEL_ADDRESS

    try:
        res = requests.get(url, headers=headers, timeout=app.MODEL_GET_TIMEOUT)
        color_list = res.json()
    except Exception as err:
        log.info(f'failed to get an all predicted color: {err}')
        color_list = ["","",""]

    return color_list

def get_current_predicted_color(cycleid,ballid,sorting_index):

    # 排出カウントが 2/5の時、オーブン工程では次のサイクルIDのボールが対象になる
    if sorting_index % 3 == 2 and ( sorting_index !=-1 and sorting_index != 8) and cycleid != 2:
        cycleid = cycleid + 1
        ballid = 0
    else:
        if ballid == 2:
            ballid = 0
        else:
            ballid = ballid + 1

    url = app.MODEL_ADDRESS + "/model_list/" + str(cycleid)

    try:
        res = requests.get(url, headers=headers, timeout=app.MODEL_GET_TIMEOUT)
        color_list = res.json()
        color = color_list[ballid]
    except Exception as err:
        log.info(f'failed to get a current predicted color: {err}')
        color = ""

    return color

# MQTT pubするデータ
def create_value(plc,led_state):
    value = {
        'time': plc.data['time'],
        'auto-drive-mode': plc.factory_start_trigger(),
        'arm-position': plc.check_arm_position(),
        'ws_index': plc.check_ws_index(),
        'cycleid': cycleid,
        'ballid': ballid,
        'predicted_color_list': predicted_color_list,
        'current_predicted_color': current_predicted_color,
        'result_color_list': result_color_list,
        'current_result_color': current_result_color,
        'ws': plc.data['ws'],
        'oven': plc.data['oven'],
        'sorting': plc.data['sorting'],
        'sorting_index': plc.check_sorting_index(),
        'led':
            {
                'time': datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
                'color': current_predicted_color,
                'status': led_state
            }
    }
    value_json = json.dumps(value).encode('utf-8')
    return value_json

# PLCデータに基づく制御
def check_plc_data( plc, led ):
    global factory_start_flag, arm_position
    global ws_index, sorting_index
    global cycleid, ballid
    global result_color_list, predicted_color_list
    global current_predicted_color, current_result_color

    factory_start_flag = plc.factory_start_trigger()
    arm_position = plc.check_arm_position()
    cycleid = plc.check_cycleid()
    ballid = plc.check_ballid()
    ws_index = plc.check_ws_index()
    sorting_index = plc.check_sorting_index()

    if ws_index < 0:
        result_color_list = [["","",""],["","",""],["","",""]]

    # LED　点灯制御
    oven_state = plc.data['oven']
    predicted_color_list = get_all_predicted_color()
    current_predicted_color = get_current_predicted_color(cycleid,ballid,sorting_index)

    if oven_state == True:
        led.turn_on(current_predicted_color)
    elif oven_state == False:
        led.turn_off(current_predicted_color)

    led_state = led.state[current_predicted_color]
    current_result_color, result_color_list = plc.get_result_color()

    app.publish(create_value(plc,led_state))
    log.info(f'auto: {factory_start_flag}, arm-position: {arm_position}, ws_index: {ws_index}, cycleid: {cycleid}, ballid: {ballid}, oven: {oven_state}, predicted_color: {predicted_color_list}, result_color: {result_color_list}, led_state: {led_state}, sorting_index: {sorting_index}')