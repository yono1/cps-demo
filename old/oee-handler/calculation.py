#!/usr/bin/env python3
from datetime import datetime
import logging
import json
import math
import app

# variant
cycleid=0
ballid=0
led_good_count=0
led_bad_count=0
oven = False
availability=0.0
performance=0.0
quality=0.0
quality_tmp=0.0
predicted_color_list = [[],[],[]]
result_color_list = [[],[],[]]
led_time_list = []
start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
current_time = start_time
led_count = 0
led_count_flag = True

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

class PLC:
    def __init__(self, data):
        self.data = data

    def get_time(self):
        global time
        try:
            time = self.data['time']
        except KeyError:
            time = ''
        return time

    def get_ws_index(self):
        global ws_index
        try:
            ws_index = self.data['ws_index']
        except KeyError:
            ws_index = -1
        return ws_index

    def get_sorting_index(self):
        global sorting_index
        try:
            sorting_index = self.data['sorting_index']
        except KeyError:
            sorting_index = -1
        return sorting_index

    def get_cycleid(self):
        global cycleid
        try:
            cycleid = self.data['cycleid']
        except KeyError:
            cycleid = 0
        return cycleid

    def get_ballid(self):
        global ballid
        try:
            ballid = self.data['ballid']
        except KeyError:
            ballid = -1
        return ballid

    def get_oven(self):
        global oven
        try:
            oven = self.data['oven']
        except KeyError:
            oven = False
        return oven

    def get_factory_start_trigger(self):
        global factory_start_flag
        try:
            factory_start_flag = self.data['auto-drive-mode']
        except KeyError:
            factory_start_flag = False
        return factory_start_flag

    def get_predicted_color_list(self):
        global predicted_color_list
        try:
            predicted_color_list = self.data['predicted_color_list']
        except KeyError:
            predicted_color_list = [["","",""],["","",""],["","",""]]
        return predicted_color_list

    def get_result_color(self):
        global result_color_list
        try:
            result_color_list = self.data['result_color_list']
        except KeyError:
            result_color_list = [["","",""],["","",""],["","",""]]
        return result_color_list

    def get_led_data(self):
        try:
            list = self.data['led']
            time = list['time']
            state = list['status']
            color = list['color']
        except KeyError:
            time = ''
            state = ''
            color = ''
        return time, state, color

# OEE topicでMQTT pubするデータ
def create_value_file( oee: float, availability: float, performance: float, quality: float):

    value = {
        'time': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
        'oee': oee,
        'availability': availability,
        'performance': performance,
        'quality': quality
        }

    if app.DEBUG:
        log.info(f'[kafka debug] Sending message with value -> {value}')
    value_json = json.dumps(value).encode('utf-8')
    return value_json

# Availabilityの計算
def validate_availability( factory_start_flag, ws_index, oven):
    global availability

    # initialize
    if ws_index < 0:
        if availability == 1.0:
            if factory_start_flag is False:
                availability = 0.0
        else:
            availability = 0.0

    if oven:
        availability = 1.0

    log.info(f'[Availability] Oven status is {oven}')
    log.info(f'[Availability] Availability: {availability * 100}%')
    return availability

def validate_performance( factory_start_flag, ws_index, oven):

    global performance

    if ws_index < 0:
        if performance == 1.0:
            if factory_start_flag is False:
                performance = 0.0
        else:
            performance = 0.0

    if oven:
        performance = 1.0

    log.info(f'[Performance] Oven status is {oven}')
    log.info(f'[Performance] Performance: {performance * 100}%')

    return performance

def deep_validation( index: int, ok: int, ng: int,  predicted_color_list: list, result_color: list):
    predicted_color = predicted_color_list[index]

    for color_index,color in enumerate(result_color):
        if color:
            if predicted_color[color_index] == color:
                ok += 1
                log.info(f'[Quality] quality{index}-{color_index} is ok{ok}: {predicted_color[color_index]} <-> {color}')
            else:
                ng += 1
                log.info(f'[Quality] quality{index}-{color_index} is ng{ng}: {predicted_color[color_index]} <-> {color}')

    return ok, ng

def validate_quality( factory_start_flag: bool, ws_index, predicted_color_list: list ,result_color_list: list):

    global quality

    # initialize
    if ws_index < 0 and factory_start_flag:
        quality = 0.0

    if result_color_list:
        ok = 0
        ng = 0
        for index,result_color in enumerate(result_color_list):
            if result_color:
                ok, ng = deep_validation(index, ok, ng, predicted_color_list, result_color)

    if (ok + ng) == 0:
        quality = 0.0
    else:
        quality = ( float(ok) )/ float( ok + ng )

    # quality = (quality + quality_tmp) / ( cycleid + 1 )
    log.info(f'[Quality] quality: {quality * 100}%')
    return quality

def check_plc_data( plc ):
    global time, ws_index, cycleid, ballid, oven
    global predicted_color_list, result_color_list
    global led_time, led_state, led_color, led_good_count, led_bad_count

    time = plc.get_time()
    ws_index = plc.get_ws_index()
    cycleid = plc.get_cycleid()
    ballid = plc.get_ballid()
    oven = plc.get_oven()
    factory_start_flag = plc.get_factory_start_trigger()
    predicted_color_list = plc.get_predicted_color_list()
    result_color_list = plc.get_result_color()
    led_time, led_state, led_color = plc.get_led_data()

    log.info(f'start oee calculation cycleid is {cycleid}, ballid is {ballid}')
    availability = validate_availability( factory_start_flag, ws_index, oven )
    performance = validate_performance( factory_start_flag, ws_index, oven )
    quality = validate_quality( factory_start_flag, ws_index, predicted_color_list, result_color_list )
    oee = availability * performance * quality

    availability = math.ceil(availability * 100)
    performance = math.ceil(performance * 100)
    quality = math.ceil(quality * 100)
    oee = math.ceil(oee * 100)

    log.info(f'[result] oee: {oee}%, availability:{availability}%, performance: {performance}%, quality: {quality}%')
    value_json = create_value_file( oee, availability, performance, quality )
    app.publish(value_json)
