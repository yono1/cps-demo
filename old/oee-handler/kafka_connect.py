#!/usr/bin/env python3
from kafka import KafkaProducer
from kafka import KafkaConsumer
import logging
import json
import app
import calculation

# Loggerを初期化
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                    level=logging.INFO)
log = logging.getLogger(__name__)

def publish_to_kafka(jsondata):
    producer.send(app.PUB_TOPIC, jsondata)
    log.info(f"write to Kafka")

def start():
    global producer

    # Kafka接続
    producer = KafkaProducer(
        bootstrap_servers=[app.KAFKA_BOOTSTRAP_SERVER])

    consumer = KafkaConsumer(
        app.SUB_TOPIC,
        bootstrap_servers=[app.KAFKA_BOOTSTRAP_SERVER])

    for msg in consumer:
        plc = calculation.PLC (json.loads(msg.value))
        calculation.check_plc_data( plc )