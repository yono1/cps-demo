import os
from flask import Flask, render_template, request, redirect, url_for
import psycopg2
from psycopg2 import sql
from datetime import datetime
from dotenv import load_dotenv

app = Flask(__name__)


# Load environment variables from .env file
load_dotenv()

# Get the value of a specific environment variable
global DB_HOST
global DB_PORT
global DB_NAME
global DB_USER
global DB_PASS

DB_HOST = os.getenv('DB_HOST')
DB_PORT = int(os.getenv('DB_PORT'))
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')


def get_db_connection():
    conn = psycopg2.connect(host=DB_HOST, port=DB_PORT, database=DB_NAME, user=DB_USER, password=DB_PASS)
    return conn

def create_tables():
    conn = get_db_connection()
    try:
        cursor = conn.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS demo (
                id SERIAL PRIMARY KEY,
                lot_no INTEGER UNIQUE NOT NULL CHECK (lot_no BETWEEN 1 AND 9),
                deadline TIMESTAMP NOT NULL,
                delivery_status BOOLEAN DEFAULT FALSE,
                heat_status BOOLEAN DEFAULT FALSE,
                heat_window INTEGER NOT NULL DEFAULT 30 CHECK (heat_window BETWEEN 30 AND 200),
                polishing_window INTEGER NOT NULL DEFAULT 20 CHECK (polishing_window BETWEEN 20 AND 200),
                update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )
        ''')
        conn.commit()
    except Exception as e:
        print(f"Error creating tables: {e}")
    finally:
        cursor.close()
        conn.close()

@app.route('/', methods=['GET'])
def index():
    error_message = None

    # データベースからデータを取得
    conn = get_db_connection()
    if conn is None:
        return render_template('index.html', rows=[], error_message="データベース接続エラーが発生しました。")

    try:
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM demo ORDER BY deadline;')
        rows = cursor.fetchall()
    except Exception as e:
        return render_template('index.html', rows=[], error_message="データベース接続エラーが発生しました。")
    finally:
        cursor.close()
        conn.close()

    return render_template('index.html', rows=rows, error_message=error_message)


@app.route('/update', methods=['GET', 'POST'])
def update():
    create_tables()  # テーブルを確認・作成する

    # 現在のUTC時刻を取得
    utc_now = datetime.now()
    default_deadline = utc_now.strftime('%Y-%m-%dT%H:%M')  # 分までの形式

    error_message = None

    if request.method == 'POST':
        lot_no = request.form.get('lot_no', type=int)
        deadline = request.form.get('deadline')
        delivery_status = request.form.get('delivery_status') == 'on'
        heat_status = request.form.get('heat_status') == 'on'
        heat_window = request.form.get('heat_window', type=int)
        polishing_window = request.form.get('polishing_window', type=int)

        # Lot Noが無効な場合
        if lot_no < 1 or lot_no > 9:
            error_message = "Lot Noは1から9の範囲内である必要があります。"
        elif lot_no and deadline:
            conn = get_db_connection()
            if conn is None:
                error_message = "データベース接続エラーが発生しました。"
            else:
                try:
                    cursor = conn.cursor()

                    # ロット No が存在するか確認
                    cursor.execute('SELECT * FROM demo WHERE lot_no = %s;', (lot_no,))
                    existing_record = cursor.fetchone()

                    # 現在のUTC時刻を取得
                    utc_now = datetime.now()

                    if existing_record:
                        # 更新処理
                        cursor.execute('''
                            UPDATE demo SET deadline = %s, delivery_status = %s, heat_status = %s, heat_window = %s, polishing_window = %s, update_time = %s
                            WHERE lot_no = %s;
                        ''', (datetime.fromisoformat(deadline), delivery_status, heat_status, heat_window, polishing_window, utc_now, lot_no))
                    else:
                        # 新規挿入処理
                        cursor.execute('''
                            INSERT INTO demo (lot_no, deadline, delivery_status, heat_status, heat_window, polishing_window, update_time)
                            VALUES (%s, %s, %s, %s, %s, %s, %s);
                        ''', (lot_no, datetime.fromisoformat(deadline), delivery_status, heat_status, heat_window, polishing_window, utc_now))

                    conn.commit()
                except Exception as e:
                    error_message = f"データベースエラーが発生しました: {e}"
                finally:
                    cursor.close()
                    conn.close()

                return redirect(url_for('update'))  # redirect先のメソッドがGETである必要があります

    # データベースからデータを取得
    conn = get_db_connection()
    if conn is None:
        return render_template('update.html', rows=[], default_deadline=default_deadline, error_message="データベース接続エラーが発生しました。")

    try:
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM demo ORDER BY deadline;')
        rows = cursor.fetchall()
    except Exception as e:
        return render_template('update.html', rows=[], default_deadline=default_deadline, error_message="データベース接続エラーが発生しました。")
    finally:
        cursor.close()
        conn.close()

    return render_template('update.html', rows=rows, default_deadline=default_deadline, error_message=error_message)


@app.route('/get_lot_data', methods=['GET'])
def get_lot_data():
    lot_no = request.args.get('lot_no', type=int)

    if not lot_no or lot_no < 1 or lot_no > 9:
        return {'error': '無効なロット番号です'}, 400

    conn = get_db_connection()
    if conn is None:
        return {'error': 'データベース接続エラーが発生しました'}, 500

    try:
        cursor = conn.cursor()

        # ロット No に対応するデータを取得
        cursor.execute('SELECT lot_no, deadline, delivery_status, heat_status, heat_window, polishing_window FROM demo WHERE lot_no = %s;', (lot_no,))
        record = cursor.fetchone()

        if not record:
            return {'error': '指定されたロット番号にはデータがありません'}, 404

        # データをJSON形式で返す
        return {
            'lot_no': record[0],
            'deadline': record[1].strftime('%Y-%m-%dT%H:%M'),  # datetime-local用フォーマット
            'delivery_status': record[2],
            'heat_status': record[3],
            'heat_window': record[4],
            'polishing_window': record[5]
        }
    except Exception as e:
        return {'error': f"データベースエラーが発生しました: {e}"}, 500
    finally:
        cursor.close()
        conn.close()


if __name__ == '__main__':
    app.run(debug=True)