# Red Hat Summit: Connect 2023 CPSデモ (プライベートリポジトリ)

## デモエリア
testtesttest2

## コンテナイメージのビルド

```bash
./build.sh <Your Docker Registry Account Name> <Your Docker Registry Password> <option: Specific Service Name listed on image.list(default: all)> <op: Docker Image tag(default: v1)>
```

## アプリケーション

```bash
.
├── ledblink-api　# ラズパイと接続されたLEDをGPIOで点灯制御するAPI
├── ledblink-monitor # ミニチュア工場のPLCデータを元にLED点灯制御をモニターするハンドラー
├── model_gen # 加工順番予測モデルの生成API
├── model_get # 加工順番予測モデルの取得・生成のエンドポイント
├── nginx-rtmp # (optional) 加工順番予測の元ネタとなる画像を取得するためのカメラの接続先RTMPエンドポイント
├── oee-handler # OEE計算ハンドラー
├── plc-data-simulator # PLCのデータ変化をシミュレーションするテストプログラム
└── quality-handler # 加工順番品質を監視し、品質劣化と判断したら加工順番予測モデルの再生成をリクエストするハンドラー

```