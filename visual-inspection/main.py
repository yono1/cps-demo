import cv2
import json
import os
import numpy as np
from flask import Flask, Response, jsonify, render_template, request
import threading

app = Flask(__name__)

# 環境変数からカメラデバイスの設定を取得
camera_device = os.getenv('CAMERA_DEVICE', 0)  # デフォルトは/dev/video0
if camera_device == '0':
    camera_device = 0


# 環境変数から色の閾値を取得（JSON形式で指定）
color_ranges_env = os.getenv('COLOR_RANGES', '{"red": [[155,25,0], [179,255,255]], "blue": [[110,50,50], [130,255,255]], "white": [[0,0,200], [255,55,255]]}')
color_ranges = json.loads(color_ranges_env)

# カメラデバイスの設定
camera = cv2.VideoCapture(camera_device)
# スレッドロックの初期化
lock = threading.Lock()

def detect_color(frame):
    """カメラフレームから赤、青、白、緑の色を検出し、バウンディングボックスとラベルを描画する"""
    h›v = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # print(color_ranges)

    for color, (lower, upper) in color_ranges.items():
        lower_bound = np.array(lower)
        upper_bound = np.array(upper)

        mask = cv2.inRange(hsv, lower_bound, upper_bound)

        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            area = cv2.contourArea(contour)
            if area > 100:
                x, y, w, h = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(frame, f"{color}", (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    return frame

def find_dominant_color(image,color_ranges):
    """画像内で最も支配的な色を検出する"""
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    color_ranges_red = color_ranges["red"]
    color_ranges_blue = color_ranges["blue"]
    color_ranges_white = color_ranges["white"]

    # Define the color ranges for red, blue, and white
    red_lower = np.array(color_ranges_red[0],dtype=np.uint8)
    red_upper = np.array(color_ranges_red[1],dtype=np.uint8)
    blue_lower = np.array(color_ranges_blue[0],dtype=np.uint8)
    blue_upper = np.array(color_ranges_blue[1],dtype=np.uint8)
    white_lower = np.array(color_ranges_white[0],dtype=np.uint8)
    white_upper = np.array(color_ranges_white[1],dtype=np.uint8)

    # Create masks for red, blue, and white
    red_mask = cv2.inRange(hsv_image, red_lower, red_upper)
    blue_mask = cv2.inRange(hsv_image, blue_lower, blue_upper)
    white_mask = cv2.inRange(hsv_image, white_lower, white_upper)

    # Count the number of pixels in each mask
    red_pixels = cv2.countNonZero(red_mask)
    blue_pixels = cv2.countNonZero(blue_mask)
    white_pixels = cv2.countNonZero(white_mask)

    # Determine the main color based on the number of pixels in each mask
    if red_pixels > blue_pixels and red_pixels > white_pixels:
        main_color = "red"
    elif blue_pixels > red_pixels and blue_pixels > white_pixels:
        main_color = "blue"
    elif white_pixels > red_pixels and white_pixels > blue_pixels:
        main_color = "white"
    else:
        main_color = "others"

    return main_color


def generate_frames(detect_colors=False):
    """カメラからの画像をフレームとして生成し、必要に応じて色検出を行う"""
    global lock
    while True:
        with lock:
            success, frame = camera.read()  # カメラからフレームを取得
            if not success:
                break

            if detect_colors:
                # フレームに色検出を適用
                frame = detect_color(frame)

            # JPEGにエンコード
            _, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/')
def index():
    """メインページの表示"""
    # 環境変数から色範囲を取得する
    return render_template('index.html', color_ranges=color_ranges)



@app.route('/video_feed')
def video_feed():
    """カメラの映像をそのままMJPEGでストリーミング"""
    return Response(generate_frames(detect_colors=False), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/processed_feed')
def processed_feed():
    """色検出済みのカメラ映像をMJPEGでストリーミング"""
    return Response(generate_frames(detect_colors=True), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/detect_color', methods=['GET'])
def detect_color_endpoint():
    """カメラフレームを使用して色を検出し、支配的な色を返すエンドポイント"""
    global lock
    global color_ranges
    try:
        with lock:
            success, frame = camera.read()  # カメラからフレームを取得
            frame = detect_color(frame)
            if not success:
                return jsonify({"error": "Failed to capture image from camera"}), 500

            # ローカルに画像を保存
            image_path = 'detected_image.png'
            cv2.imwrite(image_path, frame)

            # 支配的な色を検出
            dominant_color_label = find_dominant_color(frame,color_ranges)

            # JSONレスポンスを生成
            return jsonify({
                "dominant_color": dominant_color_label,
                "image_path": f"/{image_path}"
            })
    except Exception as e:
        print(f"Error processing request: {str(e)}")
        return jsonify({"error": "Failed to process the image"}), 500

@app.route('/detected_image.png')
def serve_image():
    """保存された画像を提供するエンドポイント"""
    return Response(open('detected_image.png', 'rb').read(), mimetype='image/png')

@app.route('/update_ranges', methods=['POST'])
def update_ranges():
    """設定された色の範囲を更新するエンドポイント"""
    global color_ranges
    color_ranges = json.loads(request.form['color_ranges'])
    print(color_ranges)
    return jsonify({"message": "Color ranges updated successfully"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
