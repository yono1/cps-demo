from fastapi import FastAPI
from typing import List

# A 2D list balls_predict_model is defined with three sublists, each containing three string elements.
# Two POST routes are defined using the @app.post decorator. The first route is /model_list/{index}, which accepts an index parameter and returns the sublist at the specified index in balls_predict_model. The sublist is returned as a flattened 1D list of strings.
# The second route is /update_model, which accepts a JSON payload containing a new 2D list of strings. The balls_predict_model variable is updated with the new list.

app = FastAPI()

balls_predict_model = [
    ["white", "white", "white"],
    ["red", "red", "red"],
    ["blue", "blue", "blue"]
]

@app.get("/")
async def all():
    model_list = balls_predict_model
    return model_list

@app.get("/model_list/{index}")
async def model_list(index: int) -> List[str]:
    model_list1d = balls_predict_model[index]
    return model_list1d

@app.post("/update_model")
async def update_model(new_model: List[List[str]]) -> str:
    global balls_predict_model
    balls_predict_model = new_model
    return "Model updated successfully"