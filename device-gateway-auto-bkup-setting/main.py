import os
import re
import subprocess
import time
import shutil
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# 環境変数から設定を取得
WATCH_DIR = os.getenv('WATCH_DIR', '/watch')  # 監視対象ディレクトリ
FILE_REGEX = os.getenv('FILE_REGEX', r'export.*\.dxg')  # 正規表現でファイル名を指定
GIT_URL = os.getenv('GIT_URL','https://gitlab.com/yono1/dgw-auto-config-setting-files')  # GitのURL
GIT_USERNAME = os.getenv('GIT_USERNAME','YOUR_USERNAME')  # Gitのユーザー名
GIT_PASSWORD = os.getenv('GIT_PASSWORD','YOUR_PASSWORD')  # Gitのパスワード
GIT_BRANCH = os.getenv('GIT_BRANCH', 'main')  # Gitのブランチ
COMMIT_MESSAGE = os.getenv('COMMIT_MESSAGE', 'Add new file(s)')  # コミットメッセージ
GIT_CLONE_DIR = os.getenv('GIT_CLONE_DIR', '/repo')  # Gitリポジトリのローカルディレクトリ
GIT_PATH = os.getenv('GIT_PATH', 'dgw-settings')  # Gitリポジトリ内のファイルコピー先ディレクトリ

# 認証情報がある場合、Git URLに認証情報を含める
def construct_git_url():
    if GIT_USERNAME and GIT_PASSWORD:
        return GIT_URL.replace('https://', f'https://{GIT_USERNAME}:{GIT_PASSWORD}@')
    return GIT_URL

git_url_with_credentials = construct_git_url()

# 正規表現をコンパイル
file_pattern = re.compile(FILE_REGEX)

class NewFileHandler(FileSystemEventHandler):
    def __init__(self, git_dir, git_path):
        super().__init__()
        self.git_dir = git_dir
        self.git_path = git_path
        self.seen_files = set()

    def on_created(self, event):
        if event.is_directory:
            return

        file_name = os.path.basename(event.src_path)
        if file_pattern.match(file_name) and event.src_path not in self.seen_files:
            print(f"New file detected: {file_name}")
            self.seen_files.add(event.src_path)
            self.copy_and_push_to_git(event.src_path, COMMIT_MESSAGE)

    def on_deleted(self, event):
        if event.is_directory:
            return

        file_name = os.path.basename(event.src_path)
        if file_pattern.match(file_name):
            print(f"File deleted: {file_name}")
            self.remove_and_push_to_git(file_name)

    def on_moved(self, event):
        if not event.is_directory:
            old_file_name = os.path.basename(event.src_path)
            new_file_name = os.path.basename(event.dest_path)
            if file_pattern.match(old_file_name) or file_pattern.match(new_file_name):
                print(f"File renamed from {old_file_name} to {new_file_name}")
                # Gitから古いファイルを削除し、新しいファイルを追加
                self.remove_and_push_to_git(old_file_name)
                shutil.copy(event.dest_path, os.path.join(self.git_dir, GIT_PATH, new_file_name))
                self.copy_and_push_to_git(event.dest_path,"Rename the file")

    def copy_and_push_to_git(self, file_path, COMMIT_MESSAGE):
        try:
            # コピー先のディレクトリを作成
            dest_dir = os.path.join(self.git_dir, self.git_path)
            os.makedirs(dest_dir, exist_ok=True)

            # ファイルをコピー
            dest_file_path = os.path.join(dest_dir, os.path.basename(file_path))
            shutil.copy(file_path, dest_file_path)
            print(f"Copied {file_path} to {dest_file_path}")

            # Git add
            subprocess.run(['git', '-C', self.git_dir, 'add', dest_file_path], check=True)

            # Git commit
            subprocess.run(
                ['git', '-C', self.git_dir, 'commit', '-m', COMMIT_MESSAGE],
                check=True
            )

            # Git push
            self.git_push_with_retry()

            print(f"File {dest_file_path} has been pushed to Git.")
        except subprocess.CalledProcessError as e:
            print(f"Error pushing file to Git: {e}")
        except Exception as e:
            print(f"Error: {e}")

    def remove_and_push_to_git(self, file_name):
        try:
            # Gitリポジトリからファイルを削除
            dest_file_path = os.path.join(self.git_dir, self.git_path, file_name)
            if os.path.exists(dest_file_path):
                os.remove(dest_file_path)
                print(f"Deleted {dest_file_path} from local Git repo")

                # Git rm
                subprocess.run(['git', '-C', self.git_dir, 'rm', dest_file_path], check=True)

                # Git commit
                subprocess.run(
                    ['git', '-C', self.git_dir, 'commit', '-m', f"Remove {file_name}"],
                    check=True
                )

                # Git push
                self.git_push_with_retry()

                print(f"File {dest_file_path} deletion has been pushed to Git.")
            else:
                print(f"File {dest_file_path} not found in Git repository")
        except subprocess.CalledProcessError as e:
            print(f"Error pushing deletion to Git: {e}")
        except Exception as e:
            print(f"Error: {e}")

    def git_push_with_retry(self):
        """Git pushを実行し、失敗した場合にgit pullをして再試行"""
        try:
            subprocess.run(
                ['git', '-C', self.git_dir, 'push', git_url_with_credentials, GIT_BRANCH],
                check=True
            )
        except subprocess.CalledProcessError:
            print("git push failed, attempting git pull and retrying push...")
            # git pullをしてリモートの最新状態を取得
            try:
                subprocess.run(
                    ['git', '-C', self.git_dir, 'pull', '--rebase'],
                    check=True
                )
                # git pushを再試行
                subprocess.run(
                    ['git', '-C', self.git_dir, 'push', git_url_with_credentials, GIT_BRANCH],
                    check=True
                )
                print("git push succeeded after retry.")
            except subprocess.CalledProcessError as e:
                print(f"Error during git pull and retry push: {e}")

def clone_or_pull_repo(git_url, git_dir):
    # Gitリポジトリが存在する場合は削除
    if os.path.exists(git_dir):
        print(f"Deleting existing repository at {git_dir}...")
        shutil.rmtree(git_dir)

    # クローン処理
    print(f"Cloning repository {git_url_with_credentials} into {git_dir}...")
    subprocess.run(['git', 'clone', git_url_with_credentials, git_dir], check=True)

def update_git_remote(git_url, git_dir):
    """GitのリモートURLを最新の認証情報に基づいて更新"""
    try:
        subprocess.run(['git', '-C', git_dir, 'remote', 'set-url', 'origin', git_url], check=True)
        print(f"Updated Git remote URL to {git_url}")
    except subprocess.CalledProcessError as e:
        print(f"Error updating Git remote URL: {e}")

def main():
    global git_url_with_credentials  # global宣言

    # Gitリポジトリのクローンまたは既存のリポジトリの使用
    clone_or_pull_repo(git_url_with_credentials, GIT_CLONE_DIR)

    # ディレクトリ監視の設定
    event_handler = NewFileHandler(GIT_CLONE_DIR, GIT_PATH)
    observer = Observer()
    observer.schedule(event_handler, WATCH_DIR, recursive=False)

    # 監視を開始
    print(f"Starting to monitor directory: {WATCH_DIR}")
    observer.start()
    try:
        while True:
            # 認証情報が更新されたか確認
            updated_git_url = construct_git_url()
            if updated_git_url != git_url_with_credentials:
                print("Detected update in Git credentials. Updating Git remote URL...")
                git_url_with_credentials = updated_git_url
                update_git_remote(git_url_with_credentials, GIT_CLONE_DIR)

            # Gitリポジトリのディレクトリが存在しない場合に再クローン
            if not os.path.exists(GIT_CLONE_DIR):
                print(f"{GIT_CLONE_DIR} is missing. Re-cloning the repository...")
                clone_or_pull_repo(git_url_with_credentials, GIT_CLONE_DIR)

            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

if __name__ == "__main__":
    main()
