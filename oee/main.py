import os
import paho.mqtt.client as mqtt
import json
import time
from datetime import datetime, timedelta
import logging
from dotenv import load_dotenv


# Load environment variables from .env file
load_dotenv()

# Get the value of a specific environment variable
global PUB_TOPIC
global SUB_TOPIC
global MQTT_PORT
global MQTT_BROKER_ADDRESS
global STANDARD_CYCLE_TIME

PUB_TOPIC = os.getenv('PUB_TOPIC')
SUB_TOPIC = os.getenv('SUB_TOPIC')
MQTT_PORT = int(os.getenv('MQTT_PORT'))
MQTT_BROKER_ADDRESS = os.getenv('MQTT_BROKER_ADDRESS')
STANDARD_CYCLE_TIME = float(os.getenv('STANDARD_CYCLE_TIME'))

DATA_DIR = './data'  # 計算結果を保存するディレクトリ
DATA_FILE = os.path.join(DATA_DIR, 'calculation_results.json')  # 計算結果を保存するファイル

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# 初期化関数
def reset_values():
    global drive_mode_start_time, start_mode_start_time, total_drive_time, total_running_time, drive_count
    global availability, performance_availability, production_count, quality, oee
    global ok_count, ng_count,desired_color

    drive_count = 0 # 設備稼働数
    drive_mode_start_time = None
    start_mode_start_time = None
    total_drive_time = timedelta(0)  # 稼働時間の合計を初期化
    total_running_time = timedelta(0)  # 時間稼働時間の合計
    availability = 0.0
    performance_availability = 0.0
    quality = 0.0
    oee = 0.0
    production_count = 0  # 生産数量
    ok_count = 0  # OKの累計
    ng_count = 0  # NGの累計
    desired_color = "" # 加熱色


# 計算結果をファイルに保存
def save_data():
    tmp_data = {
        "drive_count": drive_count,
        "total_drive_time": total_drive_time.total_seconds(),
        "total_running_time": total_running_time.total_seconds(),
        "availability": availability,
        "performance_availability": performance_availability,
        "production_count": production_count,
        "desired_color": desired_color,
        "ok_count": ok_count,
        "ng_count": ng_count
    }
    # ディレクトリの作成
    os.makedirs(DATA_DIR, exist_ok=True)  # ディレクトリがない場合は作成
    with open(DATA_FILE, 'w') as f:
        json.dump(tmp_data, f)
    log.info("Data saved to file.")

# ファイルから計算結果を読み込み
def load_data():
    global total_running_time, total_drive_time, drive_count, availability
    global performance_availability, production_count, ok_count, ng_count, desired_color

    if os.path.exists(DATA_FILE):
        with open(DATA_FILE, 'r') as f:
            tmp_data = json.load(f)
            drive_count = tmp_data.get("drive_count", 0.0)
            total_running_time = timedelta(seconds=tmp_data.get("total_running_time", 0))
            total_drive_time = timedelta(seconds=tmp_data.get("total_drive_time", 0))
            availability = tmp_data.get("availability", 0.0)
            performance_availability = tmp_data.get("performance_availability", 0.0)
            production_count = tmp_data.get("production_count", 0)
            desired_color = tmp_data.get("desired_color")
            ok_count = tmp_data.get("ok_count", 0)
            ng_count = tmp_data.get("ng_count", 0)
        log.info(f"Data loaded from file. {tmp_data}")
    else:
        log.info("No previous data found. Starting fresh.")


# 初期化
reset_values()
load_data()

# MQTTブローカーに接続したときに呼び出されるコールバック
def on_connect(client, userdata, flags, rc):
    log.info(f"Connected with result code {rc}")
    client.subscribe(SUB_TOPIC)  # 必要なトピックをサブスクライブ

# メッセージを受信したときに呼び出されるコールバック
def on_message(client, userdata, msg):
    global drive_count, drive_mode_start_time, start_mode_start_time, total_drive_time, total_running_time
    global availability, performance_availability, production_count, quality, oee
    global ok_count, ng_count,desired_color

    try:
        # 受信したペイロードをデコード（JSON文字列として取得）
        payload = msg.payload.decode('utf-8')
        data = json.loads(payload)
    except Exception as e:
        log.error(f"Error decoding message: {e}")
        return

    # 現在時刻を取得
    current_time = datetime.now()

    # 稼働時間 (ready)
    if data.get("process", {}).get("ready"):
        if drive_mode_start_time is None:
            # readyがTrueになった最初の時刻を記録
            drive_mode_start_time = current_time
            log.info(f"Ready mode started.{drive_mode_start_time}")
    else:
        log.info("Ready mode stopped.")
        if drive_mode_start_time is not None:
            # readyがFalseになった場合、すべての値をリセット
            log.info("Ready mode stopped. Resetting all values.")
            reset_values()
            return  # ここでリセット後に次の処理を行わないためにreturn

    # 時間稼働時間 (start)
    if data.get("process", {}).get("start"):
        if start_mode_start_time is None:
            # startがTrueになった最初の時刻を記録
            start_mode_start_time = current_time
            log.info(f"Start mode started.{start_mode_start_time}")

    if start_mode_start_time is not None:

        # オーブン加熱時に加熱した色を取得する
        if data.get("process", {}).get("heat",{}).get("color",{}).get("oven-status"):
            # 加熱した色を取得
            desired_color = data.get("process", {}).get("heat", {}).get("color", {}).get("desired", "")
            log.info(f"Desired color.{desired_color}")

            # 計算結果をファイルに保存
            save_data()

        # startがNoneでなく、仕分け処理まで完了したタイミングでOEEを計算する
        if (data.get("process", {}).get("sorting", {}).get("status", {}).get("white") 
        or data.get("process", {}).get("sorting", {}).get("status", {}).get("red") 
        or data.get("process", {}).get("sorting", {}).get("status", {}).get("blue")):
            log.info(f"Drive count: {drive_count}")
            elapsed_time = current_time - start_mode_start_time
            total_running_time += elapsed_time
            log.info(f"[Availability]: Elapsed running time for this cycle: {elapsed_time}")
            start_mode_start_time = None

            # Availabilityの計算
            if drive_mode_start_time is not None:
                # 稼働時間を計算
                total_drive_time = current_time - drive_mode_start_time
                log.info(f"[Availability]: Total drive time is {total_drive_time.total_seconds()}")

                # Availability = 時間稼働時間 ÷ 稼働時間
                if total_drive_time.total_seconds() > 0:
                    availability = total_running_time.total_seconds() / total_drive_time.total_seconds()
                    log.info(f"[Availability]: {availability:.2%}")
                else:
                    log.info("[Availability]: No drive time recorded yet.")
            else:
                log.info("[Availability]: Drive mode is not active.")

            # 性能稼働率の計算

            # 生産数量を取得
            production_count = int(data.get("process", {}).get("sorting", {}).get("count", 0))

            if total_running_time.total_seconds() > 0:
                log.info(f"Total running time: {total_running_time}")
                performance_availability = (STANDARD_CYCLE_TIME * production_count) / total_running_time.total_seconds()
                log.info(f"[Performance]: Performance: {performance_availability:.2%}")
            else:
                log.info("[Performance]: No running time recorded yet.")

            # 品質計算 (desired color と sorting color の比較)
            sorting_color = data.get("process", {}).get("sorting", {}).get("color", "")

            # 仕分け排出が実施された後に評価
            if desired_color and sorting_color:  # 両方が存在する場合
                log.info(f"[Debug]: desired_color {desired_color}, sorting color {sorting_color}")
                if desired_color == sorting_color:
                    ok_count += 1
                else:
                    ng_count += 1

            log.info(f"[Quality]: OK/ Color match.  {ok_count}, NG/ Color mismatch. {ng_count}")
            # 品質 = OK ÷ (OK + NG)
            total_count = ok_count + ng_count
            if total_count > 0:
                quality = ok_count / total_count
                log.info(f"[Quality]: {quality:.2%}")
            else:
                log.info("[Quality]: No production yet.")

            oee = availability * performance_availability * quality
            drive_count += 1

            # 計算結果をパブリッシュ
            publish_message = {
                'time': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
                'drive_count': drive_count,
                'total_drive_time': total_drive_time.total_seconds(),
                'total_running_time': total_running_time.total_seconds(),
                'oee': oee * 100,
                'availability': availability * 100,
                'performance': performance_availability * 100,
                'quality': quality * 100
                }
            client.publish(PUB_TOPIC, json.dumps(publish_message))
            log.info(f"Published message: {publish_message}")

            # 計算結果をファイルに保存
            save_data()

# MQTTクライアントのインスタンスを作成
client = mqtt.Client()

# コールバック関数を設定
client.on_connect = on_connect
client.on_message = on_message

# MQTTブローカーに接続
client.connect(MQTT_BROKER_ADDRESS, MQTT_PORT, 60)

# メッセージ受信ループの開始
client.loop_forever()