from fastapi import FastAPI
from fastapi.responses import FileResponse
import color_predict
from pathlib import Path

app = FastAPI()

@app.get("/")
async def current():
    new_model = color_predict.get_current_model()
    return new_model

@app.post("/update_all")
async def update_all():
    new_model,image_path_list = color_predict.get_new_model()
    color_predict.update_all_model(new_model)
    return new_model

@app.post("/update_next_model/{cycleid}")
async def update_next(cycleid):
    new_model,image_path_list = color_predict.get_new_model()

    # new_model = [
    #     ["red", "white", "white"],
    #     ["red", "red", "white"],
    #     ["blue", "blue", "white"]
    # ]
    new_model = color_predict.update_model(new_model, int(cycleid))
    return new_model

@app.post("/save_rtmp/")
async def save_rtmp():
    return color_predict.save_rtmp_image('tmp.jpg')

@app.get("/app/{filename:path}")
async def get_file( filename: str ):

    current = Path()
    file_path = current / filename
    response = FileResponse( path=file_path, filename=filename )

    return response