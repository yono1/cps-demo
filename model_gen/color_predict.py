from turtle import *
import cv2
import numpy as np
from dotenv import load_dotenv
import os
import json
import requests
import datetime

# Define the crop parameters
video_address=""
model_address=""
width=0
height=0
positions=[]
base_model = [
    ["red", "blue", "white"],
    ["red", "blue", "white"],
    ["red", "blue", "white"]
]

new_model = [[],[],[]]

headers = {"content-type": "application/json"}

def get_image_from_rtmp(video_address):
    # Open the video stream
    cap = cv2.VideoCapture(video_address)

    # Read a frame from the stream
    ret, frame = cap.read()

    # Release the capture
    cap.release()

    # Return the frame as an image
    return frame

def crop_image(image, x, y, width, height, image_name):
    cropped_image = image[y:y+height, x:x+width]
    cv2.imwrite(image_name, cropped_image)
    return cropped_image

def get_main_color(image):
    # Convert the image to the HSV color space
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Define the color ranges for red, blue, and white
    # red_lower = np.array([0, 50, 50])
    # red_upper = np.array([10, 255, 255])
    red_lower = np.array([155, 25, 0])
    red_upper = np.array([179, 255, 255])
    # blue_lower = np.array([100, 50, 50])
    # blue_upper = np.array([130, 255, 255])
    blue_lower = np.array([90, 64, 0])
    blue_upper = np.array([150, 255, 255])
    white_lower = np.array([0, 0, 200])
    white_upper = np.array([255, 55, 255])

    # Create masks for red, blue, and white
    red_mask = cv2.inRange(hsv_image, red_lower, red_upper)
    blue_mask = cv2.inRange(hsv_image, blue_lower, blue_upper)
    white_mask = cv2.inRange(hsv_image, white_lower, white_upper)

    # Count the number of pixels in each mask
    red_pixels = cv2.countNonZero(red_mask)
    blue_pixels = cv2.countNonZero(blue_mask)
    white_pixels = cv2.countNonZero(white_mask)

    # Determine the main color based on the number of pixels in each mask
    if red_pixels > blue_pixels and red_pixels > white_pixels:
        main_color = "red"
    elif blue_pixels > red_pixels and blue_pixels > white_pixels:
        main_color = "blue"
    elif white_pixels > red_pixels and white_pixels > blue_pixels:
        main_color = "white"
    else:
        main_color = "others"

    # Return the main color
    return main_color

def test():
    read_val_from_dotenv()
    image_path = '/Users/yono/Documents/work/cps-demo/model_gen/src/test.jpg'
    image = cv2.imread(image_path)
    new_model = []

    for index, position in enumerate(positions):
        position = json.loads(position)
        x = position["x"]
        y = position["y"]

        # Crop the image
        cropped_image = crop_image(image, x, y, width, height, "rtmp_test" + "_" + str(index) + ".jpg")

        # Get the main color of the image
        new_model.append(get_main_color(cropped_image))

    return new_model

def read_val_from_dotenv():
    # Load environment variables from .env file
    load_dotenv()
    # Get the value of a specific environment variable
    global video_address
    global model_address
    global height
    global width
    global positions
    global debug
    global timeout
    video_address = os.getenv('VIDEO_ADDRESS')
    model_address = os.getenv('MODEL_ADDRESS')
    height = int(os.getenv('RECT_HEIGHT'))
    width = int(os.getenv('RECT_WIDTH'))
    debug = bool(os.getenv('DEBUG'))
    timeout = float(os.getenv('TIMEOUT'))

    positions = []

    for i in range(9):
        data = os.getenv(f"POS{str(i)}")
        positions.append(data)

def get_new_model():
    read_val_from_dotenv()

    # t_now = datetime.datetime.now().time()
    # t_now_trim = str(t_now).replace(":","").replace(".","")
    # image_name= t_now_trim + ".jpg"
    image_name = "result.jpg"
    image_path = save_rtmp_image(image_name)
    image = cv2.imread(image_path)
    new_model = [[],[],[]]
    image_path_list = [[],[],[]]

    i = 0
    j = 0

    for index, position in enumerate(positions):

        position = json.loads(position)

        x = int(position["x"])
        y = int(position["y"])

        # Crop the image
        # cropped_image = crop_image(image, x, y, width, height, t_now_trim + "_" + str(index) + ".jpg")
        cropped_image = crop_image(image, x, y, width, height, "crop_" + str(index) + ".jpg")

        # Get the main color of the image
        new_model[i].append(get_main_color(cropped_image))
        image_path_list[i].append("/app/crop_" + str(index) + ".jpg")

        if j % 3 == 2:
            i += 1

        j += 1

        # if not debug:
            # os.remove(t_now_trim + "_" + str(index) + ".jpg")
            # os.remove("crop_" + str(index) + ".jpg")

    return new_model, image_path_list

def save_rtmp_image(image_name):
    read_val_from_dotenv()
    image = get_image_from_rtmp(video_address)
    cv2.imwrite(image_name, image)
    return os.path.abspath(image_name)

def get_current_model():
    global new_model
    read_val_from_dotenv()

    try:
        new_model = requests.get(model_address, headers="", timeout=timeout)
        new_model = new_model.json()
    except Exception as err:
        print(f'failed to get a predicted color: {err}')

    return new_model

def update_model(new_model, cycle):
    read_val_from_dotenv()
    base_model = get_current_model()

    base_model[cycle] = new_model[cycle]
    print(f'new model -> {base_model}')

    try:
        requests.post(model_address+"/update_model", data=json.dumps(base_model), timeout=timeout)
    except Exception as err:
        print(err)

    return base_model

def update_all_model(new_model):
    read_val_from_dotenv()
    print(f'new model -> {new_model}')

    try:
        requests.post(model_address+"/update_model", data=json.dumps(new_model), timeout=timeout)
    except Exception as err:
        print(err)

# test()